# 第9章 数据中心

* [数据中心概述](overview.md)
* [数据中心负载](workload.md)
* [数据中心体系结构](architecture.md)
* [数据中心效用分析](efficiency-and-cost.md)
* [数据中心性能分析](performance.md)
* [结语与展望](summary.md)
* [练习](exercises.md)

:[数据中心概述](overview.md)
:[数据中心负载](workload.md)
:[数据中心体系结构](architecture.md)
:[数据中心效用分析](efficiency-and-cost.md)
:[数据中心性能分析](performance.md)
:[结语与展望](summary.md)
:[练习](exercises.md)
