# 数据中心的效率与成本

要构建数据中心，首先需要建造一个仓库。第一个问题就是：在哪里建造？房地产代理强调位置，但对数据中心来说，位置意昧着接近因特网骨干光纤、电力成本低、环境灾难风险低(比如地震、洪水和飓风)。对于一个拥有许多数据中心的公司来说，另一个关注点是找到一个在地理上接近当前或未来因特网用户群的地方，以降低通过因特网的延迟。还有其他许多现实考虑因素，比如不动产税率。

配电与制冷的基础设施成本会让数据中心的构造成本相形见绌，所以我们把主要精力放在前者。图6-5和图6-6给出了数据中心内的配电与制冷基础设施。

尽管有许多不同的部署方式，但在北美洲，从115千伏的电力塔高压线开始，电力通常经过大约5个步骤、四级电压变换到达服务器。

(1) 变电站从115000伏转换为13200伏的中压线，效率为99.7%。

(2) 为防止整个数据中心在停电时离线，数据中心和一些服务器一样，配备有不间断电源(UPS)。在这种情况下，其中涉及大型柴油机，可以在发生紧急状况时将供电任务从电力公司接管过来，还会涉及电池或惯性轮，用于在电力服务中止但柴油机尚未准备就绪时维持供电。这些发电机和电池可能占用很大的空间，通常将它们和IT设备放在不同的房间中。UPS扮演着三个角色：电力调整(保持正常的电压和其他性能指标)、在发电机启动并正常供电时保持电力负载、在从发电机切换回公共用电时保持电力负载。这种超大型UPS的效率为94%，所以使用UPS会损失6%的电能。数据中心中UPS可能占全部IT设备成本的7%~12%。

……

图6-5 配电与发生损耗的地方

……

图6-6 制冷系统的机制设计

(3) 系统中的下一个组件是配电单元(PDU)，它将转换为480伏的三相内部低压电源。这一转换效率为98%。典型的PDU可以承载75~225千瓦的负载，相当于10个机架的负载。

(4) 还有另外一个下变换步骤，将其转换为服务器可以使用的208伏两相电源，其效率也是98%。(在服务器内部，还有另外一些步骤，将电压降到芯片可以使用的级别，见6.7节。)

(5) 连接器、断路器和至服务命电气连接的整体效率为99%。

北美之外的数据中心使用不同的变换值，但整体设计是类似的。

总而言之，将来自公共用电的115千伏电源转换为服务器可以使用的208伏电源，其效率为89%:

$$
99.7\% \times 94\% \times 98\% \times 98\% \times 99\% = 89\%
$$

这一总效率仅留下10%多一点的改进空间，但后面将会看到，工程师们仍然在尝试锦上添花。

制冷基础设施中的改进机会要大得多。计算机机房空调(CRAC)单元使用冷却水来冷却服务器室内的空间，类似于冰箱通过向外部释放热量来降低温度。当液体吸收热量时，它会蒸发。相反，当液体释放热量时，它会冷凝。空调机将液体注入低压螺旋管中，使其蒸发并服收热量，然后再将其发送到外部冷凝器，在这里释放热量。因此，在CRAC单元中，风扇吹动热空气，穿过一组装有冷水的螺旋管，水泵将加热后的水移到外部冷疑器进行冷却。服务器的冷却空气通常介于64℉与71℉之间(18和22℃之间。)图6-6显示了一大组风扇和水泵，使空气和水在整个系统中流动。

显然，提高能耗效率的最简单方法之一就是让IT设备在较高温度下运行，降低冷却空气的需求。一些数据中心在远高于71℉(22℃)的温度下运行自己的设备。

除了冷凝器之外，在一些数据中心中还会利用冷却塔，先充分利用外部的较冷空气对水进行冷却，然后再将初步冷却后的水发送给冷凝器。真正要紧的温度称为**湿球温度**。在测量湿球温度时，会向一个温度计沾有水的球形未端吹动空气。这是通过空气流动蒸发水分所能达到的最低温度。

温水流过冷却塔的一个大面积表面，通过蒸发将热传递给外部空气，从而使水冷却。这种技术被称为经济化供风。一种替换方法是使用冷水，而不是冷空气。Google公司在比利时的数据中心使用一种"水到水"中间冷却器，从工业导管取得冷水，用于冷却来自数据中心内部的热水。

IT设备本身的空气流进行了仔细规划，有些设计甚至还使用了空气流仿真器。高效的空气流设计可以减少冷空气与热空气的混合机会，从而保持冷空气的温度。例如，在机架上摆放服务器时，可以让相邻行中的服务器朝向相反方向，使排出的高温废气吹向相反方向，从而使数据中心的热空气通道和冷空气通道交替存在。

除了能量损耗之外，冷却系统还会因为蒸发或溢入下水道的原因而消耗大量冷却水。例如，8兆瓦设施每天可能使用70000到200000加仑的水。

在典型数据中心中，制冷系统与IT设备的相对功率成本[Barroso和Hölzle2009]如下：

- 冷减器占IT设备功率的30%~50%；
- CRAC占IT设备功率的10%~20%，大多消耗在风扇上。

让人奇怪的是，在减去配电设备与制冷系统的开销之后，仍然不能很清楚地看出一个数据中心可以支持多少服务器。服务器制造商提供的所谓**铭牌额定功率**总是很保守的，它是一个服务器可能消耗的最大功率。因此，第一步就是在数据中心中可能部署的各种工作负载下对一台服务器进行测量。(联网设备通常占总功耗的5%，在开始时可以忽略。)

为了确定数据中心的服务器数目，可以将IT的可用功率除以测得的服务器功率，但是，根据Fan、Weber和Barroso[2007]所说，这同样太过保守了。他们发现，由于很少有真正的工作负载可以让数千台服务器同时工作于峰值状态，所以数千台服务器理论上在最糟情况所做的工作与它们实际所做的工作之间有很大的差距。他们发现，根据单个服务器的功率，他们可以放心地将服务器数目超额认购40%。他们建议，数据中心架构师应当做一些工作，提高数据中心内部的平均功率利用率。但是，他们还建议使用大量的监视软件和安全机制，当工作负载偏移时可以取消某些较低优先级的任务。

Barroso和Hslzle[2009]对IT设备自身内部的功率利用进行了分解，据此给出了在2007年部署的 Google 数据中心 的数据，如下所示:

- 33%的功率用于处理器;
- 30%用于DRAM;
- 10%用于磁盘;
- 5%用于联网;
- 22%用于其他原因(服务器内部)。

## 测量数据中心的效率

有一种广泛使用的简单度量可以用来评估一个数据中心或数据中心的效率，称为功率利用效率(或PUE)：

$$
PUE=\frac{总设施功率}{IT设备功率}
$$

因此，PUE总是大于或等于1，PUE越大，数据中心的效率就越低。

Greenberg等人[2006]报告了19个数据中心的PUE，以及制冷基础设施开销所占的比例。图6-7展示了他们的研究成果，按照PUE从最高效到最低效排列。PUE的中值为1.69，制冷基础设施使用的功率超过服务器本身的一半，平均起来，1.69中有0.55用于制冷。注意，这些都是平均PUE，可能会根据工作负载、甚至外部空气温度而发生日常变化，稍后将会看到这一点。

……

图6-7 2006年19个数据中心的功率利用效率[Greenberg等人，2006]。在计算PUE时，以IT设备的功率为基准，对空调(AC)及其他应用(比如配电)的功率进行了归一化。因此，IT设备的功率必然为1.0，AC的功率为IT设备功率的0.3-1.4倍。"其他"功率为IT设备的0.05-0.6倍。

由于最终度量是每美元实现的性能，所以仍然需要测试性能。如前面的图6-3所示，距离数据越远，带宽越低，延迟越大。在数据中心中，服务器内部的DRAM带宽为机架内带宽的200倍，而后者又是阵列内带宽的10倍。因此，在数据中心内部放置数据和程序时，还需要考虑另一种局域性。

数据中心的设计人员经常关注带宽，而为数据中心开发应用程序的程序员还会关注延迟，因为这些延迟会让用户感受到。用户的满意度和生产效率都与服务的响应时间紧密联系在一起。在分时时代的几项研究表明：用户生产效率与交互时间成反比，这一交互时间通常被分解为人员输入时间、系统响应时间、人们在输入下一项时考虑回应的时间。试验结果表明：将系统响应时间削减30%可以将交互时间减少70%。这一令人难以相信的结果可以用人类自身的特性来解释：人们得到响应的速度越快，需要思考的时间越短，因为在这种情况下不太容易分神，一直保持"高速运转"。

表6-5给出了对Bing搜索引擎进行这一试验的结果，在此试验中，搜索服务器端插入了30ms至2000ms的延迟。和前面研究中的预测一样，到下一次单击之前的时间差不多是这一延迟的两倍，也就是说，服务器端延迟为200ms时，下一次单击之前的时间会增加500ms。收入随着延迟的增加而线性下降，用户满意度也是如此。对Google搜索引擎进行的另一项研究发现，这些影响在试验结束4周之后还没有消失。五个星期之后，当用户体验到的延迟为200ms时，每天的搜索人员会减少0.1%，当用户体验到的延迟为400ms时，每天的搜索人员会减少0.2%。考虑到搜索产生的经济效益，即使如此之小的变化也是令人不安的。事实上，这些结果的负面影响非常严重，以致这项试验提前结束[Schurman和Brutlag2009]。

表6-5 Bing搜索服务器的延迟对用户行为的负面影响(Schurman和Brutlag[2009])

|服务器延迟(ms)|下一次单击之前的增加时间(ms)|查询/用户|任意单击/用户|用户满意度|收益/用户|
|-:|-:|-:|-:|-:|-:|
|   50|   ——|    ——|    ——|    ——|    ——|
|  200|  500|    ——| -0.3%| -0.4%|    ——|
|  500| 1200|    ——| -1.0%| -0.9%| -1.2%|
| 1000| 1900| -0.7%| -1.9%| -1.6%| -2.8%|
| 2000| 3100| -1.8%| -4.4%| -3.8%| -4.3%|

由于因特网服务极端看重所有用户的满意度，所以通常在指定性能目标时，不是提供一个平均延迟目标，而是给出一个很高的百分比，要求至少有如此比例的请求低于某一延迟门限。这种门限目标被称为服务级目标(SLO)或者服务级协议(SLA)。某一SLO可能是要求99%的请求都必须低于100ms。因此，Amazon Dynamo 键值存储系统的设计者决定：为使服务能够在Dynamo的顶层提供好的延迟性能，他们的存储系统必须在99.9%的时间内实现其延迟目标[DeCandia等人2007]。例如，Dynamo的一项改进对第99.9个百分点的帮助要远多于平均情景，这反映了他们的侧重点。

## 数据中心的成本

在引言中曾经提到，数据中心的设计者与大多数架构师不同，他们要同时关注数据中心的运行成本和构建成本。会计部门将前一成本记为**运行性支出**(OPEX)，后一成本记为**资本性支出**(CAPEX)。

为了正确合理地看待能源成本，Hamilton[2010]通过一项案例研究了数据中心的成本。他确定8MW设施的CAPEX为8800万美元，大约46000台服务器和相应的网络设备向数据中心的CAPEX另外增加了7900万美元。表6-6给出了这一案例研究的其他假定。

表6-6 数据中心的案例研究，据Hamilton[2010]，四舍五入到最接近的5000美元

|                                                |                 |
|:-----------------------------------------------|----------------:|
| 设施规模(临界负载：瓦)                          |       8,000,000 |
| 平均功率利用率(%)                              |             80% |
| 功率利用效率(PUE)                              |            1.45 |
| 电力成本(美元/千瓦时)                          |        0.07美元 |
| 电力和制冷基础设施百分比(占总设施成本的百分比) |             82% |
| **设施的CAPEX(不包括IT设备)**                  |  88,000,000美元 |
| 服务器数                                       |          45,978 |
| 成本/服务器                                    |        1450美元 |
| **服务器的CAPEX**                              |  66,700,000美元 |
| 机架交换机数                                   |            1150 |
| 成本/机架交换机                                |        4800美元 |
| 阵列交换机数                                   |              22 |
| 成本/阵列交换机                                |     300,000美元 |
| L3交换机数                                     |               2 |
| 成本/L3交换机                                  |     500,000美元 |
| 边界路由器数目                                 |               2 |
| 成本/边界路由器                                |     144,800美元 |
| **网络设备的CAPEX**                            |  12,810,000美元 |
| **数据中心的总CAPEX**                          | 167,510,000美元 |
| 服务器分擅时间                                 |             3年 |
| 网络设备分捉时间                               |             4年 |
| 设施分擅时间                                   |            10年 |
| 借款的年度利率                                 |              5% |

\* 图特网带宽成本随应用程序变化，所以这里未包含在内。设施CAPEX的其余18%包括购买知识产权和机房的建设成本。我们在表6-7中加入了安全和设施管理的人力成本，此案例研究没有包括这一部分。注意，Hamilton的评估是在他加入Amazon之前完成的，这些评估值并非以特定公司的数据中心为基础。

由于美国会计规则允许我们将CAPEX转换为OPEX，所以我们现在可以为总能耗成本定价了。我们只需要将CAPEX分摊到设备有效寿命内的每个月份，使其为一个固定值。表6-6对这一案例研究的月度OPEX进行了分解。注意，不同设备的分摊率有很大不同，设施的分摊期限为10年，网络设备为4年，服务器为3年。因此，数据中心设施以10年为单位持续运行，但需要每3年将服务器更换一次，每4年将网络设备更换一次。通过分摊CAPEX，Hamilton得到了月度OPEX，包括借钱支付数据中心款项的利率(每年5%)。月度OPEX为380万美元，大约为CAPEX的2%。

利用此表，可以算出一个很便捷的准则，在决定使用哪些与能源有关的组件时，一定要记住这一准则。在一个数据中心中，每年每瓦的全额成本(包括分摊电力和制冷基础设施的成本)为：

$$
\frac{基础设施的月度成本+电力的月度成本}{设施规模(单位：瓦)} \times 12 = \frac{76.5万美元+47.5万美元}{800万} \times 12 = 1.86美元
$$

此成本大约是2美元(瓦\*年)。因此，要想通过节省能源来降低成本，花费不应当超过2美元(瓦\*年)(见6.8节)。

注意，有超过三分之一的OPEX是与电力有关的，当服务器成本随时间下降时，这一部分反而会上升。网络设备的成本也很高，占总OPEX的8%，服务器CAPEX的19%，网络设备成本不会像服务器成本那样快速下降。特别是对于那些在联网层次结构中高于机架的交换机，这一点尤其正确，大多数联网成本都花费在这些交换机上(见6.6节)。关于安全与设施管理的人力成本大约为OPEX的2%。将表6-7中的OPEX除以服务器的数目及每个月的小时数，可以得到其成本大约为每小时0.11美元/服务器。

表6-7 表6-6的月度OPEX，四舍五入到最接近的5000美元

| 费用(占总费用的百分比) | 类别               | 月度成本(美元) | 月度成本百分比 |
|:-----------------------|:------------------|---------------:|---------------:|
| 分摊CAPEX(85%)         | 服务器             |      2,000,000 |            53% |
|                        | 网络设备           |        290,000 |             8% |
|                        | 电力与制冷基础设施 |        765,000 |            20% |
|                        | 其他基础设施       |        170,000 |             4% |
| OPEX(15%)              | 月度用电成本       |        475,000 |            13% |
|                        | 月度人员薪金与津贴 |         85,000 |             2% |
| -                      | 总OPEX             |      3,800,000 |           100% |

*注意，服务器的3年分摊期意味着需要每3年购买一次新服务器，而设施的分推期为10年。因此，服务器的分摊构建成本大约是设施的3借。人力成本包桔3个安保岗位，每天连续24小时，每年365天，每人每小时为20美元；1个设施人员，每天连续24小时，每年365天，每人每小时为30美元。津贴为薪金的30%。这一计算没有包含因特网网络带宽成本，图为它是随应用程序的变化而变化的，也没有包含供应商维护费用，图为它们是随设备与协议的变化而变化的。

___
例题

美国不同地区的电力成本变化范围为0.03~0.15美元/千瓦时。这两种极端费率对每小时的服务器成本有什么样的影响？

答案

我们将8兆瓦的临界负载乘以表6-6中的PUE和平均功率利用率，以计算平均功率使用率：

$$
8 \times 1.45 \times 80\% = 9.28兆瓦
$$

于是，若费率为0.03美元/千瓦时，月度电力成本从表6-7的475,000美元变为205,000美元，若费用为0.15美元/千瓦时，则变为1,015,000美元。电力成本的这些变化使每小时的服务器成本分别由0.11美元变为0.10美元和0.13美元。
___
___
例题

如果将所有分摊时间都变为相同的(比如)5年，那月度成本会发生什么变化？每台服务器每小时成本会发生什么变化？

解答

可以从 <http://mvdirona.com/TalksAndPapers/PerspectivesDataCenterCostAndPower.xls> 下载该电子表格。将分摊时间改为5年会将表6-7的前4行变为：

|                    |               |     |
|:-------------------|--------------:|----:|
| 服务器             | 1,260,000美元 | 37% |
| 网络设备           |   242,000美元 |  7% |
| 电力与制冷基础设施 | 1,115,000美元 | 33% |
| 其他基础设施       |   245,000美元 |  7% |

总月度OPEX为3,422,000美元。如果我们每5年更换所有东西，成本将为每小时0.103美元/服务器，分摊成本的主体现在是设施，而不是服务器，如表6-7所示。
___

每小时0.11美元/服务器的费率远低于许多公司拥有和运行自有(较小)传统数据中心的成本。数据中心的成本优势导致大型因特网公司都将计算功能当作一种公用设施来提供，和电力一样，你只需要为自己使用的那一部分付费即可。今天，公用计算有一个更好的名字——云计算。
