# 数据中心的计算机体系结构

网络是将50000台服务器连接在一起的中枢。类似于第2章的存储器层次结构，数据中心使用一种层次网络结构。图6-2给出了一个示例。理想情况下，这种组合网络提供的性能应当接近于为50000台服务器定制的高端交换机，而每端口的成本则接近于为50台服务器设计的大众化交换机。在6.6节可以看到，目前的解决方案与理想情况相去甚远，数据中心的网络是一个活跃的探索领域。

19英寸(48.26厘米)机架仍然是容纳服务器的标准框架，尽管这一标准要追溯到20世纪30年代的铁路硬件。服务器的大小按照它们在机架内占用的机架单元(U)数计算。1U高1.75英寸(4.45厘米)，这是一个服务器可以占用的最小空间。

7英尺(213.36厘米)的机架提供48U，因此，针对一个机架设计的最流行交换机都是48端口以太网交换机，这就不是巧合了。这一产品已经成为一种大众化商品，2011年，1Gbit/s 以太网这接的每端口成本只有30美元[Barroso和Hölzle2009]。注意，机架内的速度对于每个服务器都是一样的，所以软件将发送器和接收器放在哪个位置都没关系，只要它们位于同一机架就行。从软件的角度来看，这种灵活性是很理想的。

这些交换机通常提供2-8个上行链接，它们离开机架，进入网络层次结构的下一层更高阶交换机。因此，离开机架的速度是机架内速度的1/6到1/24(8/48到2/48)。这一比值称为超额认购率(over subscription)。当超额认购率很高时，程序员必须知道将发送机和接收机放在不同机架时导致的性能后果。这样会增大软件调度负担，这是支持数据中心专用网络交换机的另一个理由。

……

图6-2 数据中心中的交换机层次结构。(基于Barroso和Hölzle[2013]论文中的图1-2)

## 存储

一种很自然的设计是用服务器填充一个机架，当然要扣除大众化以太网机架交换机所需要的空间。这种设计带来一个问题：把存储放在哪儿。从硬件构建的角度来看，最简单的解决方案是将磁盘包含在服务器中，通过以太网连接访问远程服务器磁盘上的信息。另一种替代方案是使用网络附属存储(NAS)，可以是通过类似于Infiniband的存储网络。NAS解决方案的每TB存储容量成本通常要更高一些，但它提供了许多功能，包括用于提高存储器可靠性的磁盘阵列(RAID)技术。

根据上一节表达的思想，可以预计：数据中心通常会依靠本地磁盘，并提供用于处理连接性和可靠性的存储软件。例如，谷歌文件系统(GFS)使用本地磁盘，至少维护三个副本，以克服可靠性问题。这一冗余不仅可以应对本地磁盘故障，还能应对机架和整个集群的电源故障。最终一致性设计给GFS提供的灵活性降低了使这些副本保持一致所需的成本，还降低了存储系统的网络带宽需求。稍后将会看到，本地访问模式还意昧着连向本地存储的高带宽。

要小心：在讨论数据中心的体系结构时，对于集群一词有一点混淆。根据6.1节的定义，数据中心就是一个超大型集群。而Barroso和Hölzle[2013]则用集群一词来表示更大一级的计算机组，在本例中大约为30个机架。在本章中，为了避免混淆，我们使用阵列一词来表示一组机架，使集群一词保持其最初含义，既可以表示一个机架内的联网计算机组，也可以表示整整一仓库的联网计算机。

## 阵列交换机

<!-- 这一段在新版本里已删除 -->

将一个机架阵列连在一起的交换机要比48口大众化以太网交换机贵得多。之所以出现这种高成本，部分原因是由于较高的连通性，部分原因是为了减轻超额认购问题，必须大幅增大通过交换机的带宽。据Barroso和Hölzle[2013]报告，如果一个交换机的二分带宽(基本上就是最糟情况下的内部带宽)是机架交换机二分带宽的10倍，那其成本将大约为100倍。其中一个原因是$n$端口交换机带宽的成本成长速度与$n^2$成比例。

<!-- 归结于公司不公开的诉求，也许是内容引起争议的原因之一 -->

这种高成本的另一原因是这些产品为其生产公司提供了很高的利润率。为了证明这种高价格的正当性，这些公司的一种做法就是提供一些诸如数据包检测之类的昂贵功能，这些功能之所以昂贵，是因为它们必须以极高速率运转。例如，网络交换机是内容可寻址存储器芯片和现场可编程门阵列(FPGA)的主要用户，这样当然有助于提供这些功能，但这些芯片本身是很昂贵的。这些功能在因特网设置上可能很有价值，但一般不会在数据中心中使用。

## 数据中心存储层次结构

表6-4给出了数据中心内部存储器层次结构的延迟、带宽和容量，图6-3以可视方式显示了同一数据。这些数字基于以下假设[Barroso和Hölzle2013]。

表6-4 数据中心存储器层次结构的延迟、带宽和容量[Barroso和Hölzle2013]

<!-- 原表内容有错且于新版已更新闪存数据，不过配图没有更新 -->

|                 |   本地 |    机架 |      阵列 |
|:----------------|-------:|--------:|----------:|
| DRAM延迟 (μs)   |    0.1 |     300 |       500 |
| 闪存延迟 (μs)   |    100 |     400 |       600 |
| 磁盘廷迟 (μs)   | 10,000 |  11,000 |    12,000 |
| DRAM带宽 (MB/s) | 20,000 |     100 |        10 |
| 闪存带宽 (MB/s) |   1000 |     100 |        10 |
| 磁盘带宽 (MB/s) |    200 |     100 |        10 |
| DRAM容量 (GB)   |     16 |    1024 |    31,200 |
| 闪存容量 (GB)   |    128 |  20,000 |   600,000 |
| 磁盘容量 (GB)   |   2000 | 160,000 | 4,800,000 |

*图6-3绘制了这一相同信息。

……

图6-3 数据中心存储器层次结构的延迟、带宽和容量数据曲线，数据与表6-4中相同[Barroso和Hölzle2013]

- 每个服务器包含16GB存储器，访问时间为100ns，传输速度为20GB/s，还有一个2TB磁盘，访问时间为10ms，传输速度为200MB/s。每块主板上有两个插槽，它们共享一个1 Gbit/s以太网端口。
- 每对机架包括一个机架交换机，容纳80个2U服务器(见6.7节)。联网软件再加上交换机开销将到DRAM的延迟增加到100ms，磁盘访问延迟增加到11ms。因此，一个机架的总存储容量大约为1TB的DRAM和160TB的磁盘存储。1 Gbit/s以太网将连向该机架内的DRAM或磁盘的远程带宽限制为100MB/s。
- 阵列交换机可以承载30个机架，所以一个阵列的存储容量增加30倍：DRAM为30TB，磁盘为4.8PB。阵列交换机硬件和软件将连向阵列内DRAM的延迟增加到300ms，磁盘延迟增加到12ms。数据交换机的带宽将连向阵列DRAM或阵列磁盘的远程带宽限制为10MB/s。

表6-4和图6-3显示：网络开销大幅延长了从本地DRAM到机架DRAM和阵列DRAM的延迟，但这两者的延迟性能仍然优于本地磁盘，不到其十分之一。网络消除了机架DRAM与机架磁盘之间、阵列DRAM和阵列磁盘之间的带宽差别。

这个数据中心需要20个阵列连接到50,000个服务器，所以联网层次结构又多了一级。图6-4给出了用于将阵列连接在一起并连至因特网的传统L3路由器。

……

图6-4 用于将阵列连接在一起并连接到因特网的L3网络[Greenberg等人，2009]。一些数据中心使用独立的边界路由器将因特网连接到数据中心L3交换机

大多数应用程序可以放在数据中心中的单个阵列上。那些需要多个阵列的应用程序会使用分片或分区，也就是说将数据集分为独立片断，然后再分散到不同阵列中。对整个数据集执行的操作被发送到托管这些片断的服务器，其结果由客户端计算机接合起来。

---

例题

假定90%的访问为服务器的本地访问，9%的访问超出服务器但在机架范围内，1%的访问超出机架但在阵列范围内，则平均存储器延迟为多少？

解答

平均存储器访问时间为:

$$
(90\% \times 0.1) + (9\% \times 100) + (1\% \times 300) = 0.09 + 9 + 3 = 12.09微秒
$$

或者说，比100%的本地访问减缓120倍以上。显然，实现一个服务器内的访问局域性对于数据中心性能来说是至关重要的。

---
---

例题

在服务器内部的磁盘之间、在机架内的服务器之间、在阵列中不同机架内的服务器之间，传递1000MB需要多少时间？在这三种情况下，在DRAM之间传递1000MB可以加快多少时间?

解答

在磁盘之间传递1000MB需要的时间为：

在服务器内部$=1000/200=5秒$

在机架内部$=1000/100=10秒$

在阵列内部$1000/10=100秒$

在存储器之间传送块时需要的时间为：

在服务器内部$=1000/20000=0.05秒$

在机架内部$=1000/100=10秒$

在阵列内部$=1000/10=100秒$

因此，对于单个服务器外部的块传输而言，由于机架交换机和阵列交换机是瓶颈所在，所以数据是在存储器中还是磁盘中并不重要。这些性能限制影响了数据中心软件的设计，并激发了对更高性能交换机的需求(见6.6节)

---

知道了IT设备的体系结构，我们现在可以看看如何对其进行摆放、供电和冷却，并讨论构建和运行整个数据中心的成本，与其中IT设备本身的成本进行对比。
